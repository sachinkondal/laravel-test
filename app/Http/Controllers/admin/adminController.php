<?php

namespace App\Http\Controllers\admin;

use \Symfony\Component\HttpFoundation\Request;
use App\User;
use Validator;
use Auth;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class adminController extends Controller
{
     use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $redirectTo = '/admin/';
    
    public function __construct()
    {
        $this->middleware('auth_admin', ['except' =>['getLogin','postLogin']]);
    }
    
    public function postLogin(Request $r) {
        
        if (Auth::attempt(['email' => $r->get('email'), 'password' => $r->get('password')])) {
            
            return redirect('admin/');
        }
    }
    public function getLogin(){
        return view('admin.login');
    }
    
    
    
    public function getIndex(){
        $hotels = \App\Hotel::all();
        return view('admin.profile' ,  ['hotels' => $hotels]);
    }
    
    public function postHotel(Request $r){
       if(\App\Hotel::create(['name'=>$r->get('name' ) , 'location'=>$r->get('location') , 'room'=>$r->get('room')])){
           echo "ok";
           
       }

    }

}
