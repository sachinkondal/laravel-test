<?php

use Illuminate\Database\Seeder;

class userSeerder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          	'id'=>1,
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
        ]);
        DB::table('users')->insert([
          	'id'=>2,
            'name' => 'john',
            'email' => 'john@gmail.com',
            'password' => bcrypt('doe'),
        ]);


    }
}
