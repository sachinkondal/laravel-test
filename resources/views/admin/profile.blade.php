@extends('layouts.app')

    @section('content')
    <form class="form-horizontal" role="form" method="POST" action="{{ url('admin/hotel') }}">
                        {!! csrf_field() !!}

                        <div>
                            <label class="col-md-4 control-label">Hotel Name</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                            </div>
                        </div>

                        <div >
                            <label class="col-md-4 control-label">location</label>

                            <div class="col-md-6">
                                <textarea class="form-control" rows='5' name="location"></textarea>

                            </div>
                        </div>

                        
                        <div >
                            <label class="col-md-4 control-label">Rooms</label>

                            <div class="col-md-6">
                                <input type='number' min="0" max="50" class="form-control"  name="room">

                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>

                               <!--  <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a> -->
                            </div>
                        </div>
                    </form>
    <table class='table'>
        <tr>
            <th>
                Name
            </th>
            <th>
               location
            </th>
            <th>
                Rooms
            </th>
            
        </tr>
        @foreach ($hotels as $h )
            <td>
                {{$h->name}}
            </td>
            <td>
               {{$h->location}}
            </td>
            <td>
                {{$h->rooms}}
            </td>
            
        @endforeach
    </table>
    @endsection